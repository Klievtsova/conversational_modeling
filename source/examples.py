import os
import json
from my_packages.initial_extraction import  model_basic_info,get_basic_information, descriptions_to_json, save_dict 
from my_packages.extract_task_from_bpmn import find_all_tasks
from my_packages.chatbot_api import extract_tasks, convert_to_list
from my_packages.evaluation_tables import compare_pd_llm, compare_llm_bpmn, task_per_task
from my_packages.augmented_set import augmentation_set
from my_packages.graph_generation import generate_graph,extract_mermaid_tasks,extract_graphviz_tasks
from my_packages.average_data import get_augment_average
import pandas as pd

# #--------------------------------------GET basic information from all BPMN models--------------------------------------------------------------------
# rootdir = '../data_set/bpmn_models/'
# output = "../test_output/bpmn_basic_info.json"
# table = {}
# for subdir, dirs, files in os.walk(rootdir):
#     for d in dirs:
#         new_root = rootdir + d + "/"
#         result = get_basic_information(new_root)
#         table[d] = result
# save_dict(table,output)

# #STEP 1 
# #--------------------------------------convert all descriptions into one json file -----------------------------------------------
# rootdir = '../data_set/initial-cases/'
# output = "../test_output/process_descriptions1.json"
# data = descriptions_to_json(rootdir)
# save_dict(data,output)

# #STEP 2
# #--------------------------------------GET task lists from bpmn models --------------------------------------------------------------------
# table = {}
# rootdir = '../data_set/bpmn_models/'
# output = "../test_output/bpmn_model_tasks1.json"
# for subdir, dirs, files in os.walk(rootdir):
#     for d in dirs:
#         table[d]= {}
#         new_root = rootdir + d + "/"
#         for subdir, dirs, files in os.walk(new_root):
#             for f in files:
#                 file_name = f.split(".")[0]
#                 path_to_file = new_root + f
#                 if ".xml" in path_to_file:   
#                     result = find_all_tasks(path_to_file)
#                     table[d][file_name] = result
# save_dict(table,output)

# #STEP 3
# #--------------------------------------GET task list from text with chat gpt --------------------------------------------------------------------
#"""input: json file from STEP 1; output: dictionary with list of tasks per process description. Single file for each gpt model"""
# list_of_chats = {"gpt3-5":"gpt-3.5-turbo"} 
# # list_of_chats = {"gpt1":"text-davinci-001","gpt2":"text-davinci-002","gpt3":"text-davinci-003","gpt3.5":"gpt-3.5-turbo","gpt4":"gpt-4"}                                      
# f = open('../test_output/process_descriptions.json')  
# data = json.load(f)
# f.close()

# for chat in list_of_chats:
#     table = {}    
#     for key in data:
#         text = data[key]
#         tasks = extract_tasks(list_of_chats[chat],text,0)    # for restricted labels use "1" insted of 0
#         table[key] = convert_to_list(tasks)
#     output = "../test_output/{}_tasks_extraction.json".format(chat)
#     save_dict(table,output)

# #STEP 4
# #-------------------------------------compare tasks from llm vs process description --(KPI1 and KPI4)------------------------------------------------------------------
#"""input: json file from STEP 1 and directory with json files from STEP 3; output: Single file with text similarity for all (process description:list of llm tasks) pairs"""
# task_dir = '../tasks/' 
# process_description = '../test_output/process_descriptions.json'

# output = "../test_output/kpi1-cos.json"
# table = compare_pd_llm(task_dir,process_description,type="cos")
# save_dict(table,output)

# output = "../test_output/kpi1-bert.json"
# table = compare_pd_llm(task_dir,process_description,type="bert")
# save_dict(table,output)

# # #STEP 5
# # #-------------------------------------compare tasks from chat vs tasks from models ----(KPI2 and KPI5)----------------------------------------------------------------
#"""input: json file from STEP 2 and directory with json files from STEP 3; output: Single file with set similarity for all (list of bpmn tasks:list of llm tasks) pairs"""
# model_tasks = "../test_output/bpmn_model_tasks.json"
# task_dir = '../tasks/' 

# output = "../test_output/kpi2_cos.json"
# table = compare_llm_bpmn(model_tasks,task_dir,type="cos")
# save_dict(table,output)

# output = "../test_output/kpi2_bert.json"
# table = compare_llm_bpmn(model_tasks,task_dir,type="bert")
# save_dict(table,output)

# #STEP 6
# #-------------------------------------compare tasks from chat vs tasks from models : task per task ----(KPI3 and KPI6)----------------------------------------------------------------
#"""input: json file from STEP 2 and directory with json files from STEP 3; output: Single file with set overlap for all (list of bpmn tasks:list of llm tasks) pairs (task per task)"""
# model_tasks = "../test_output/bpmn_model_tasks.json"
# task_dir = '../tasks/' 

# for subdir, dirs, files in os.walk(task_dir):
#     for file in files:
#         table = task_per_task(task_dir,model_tasks,file,"cos")
#         output = "../test_output/gpt2_kpi3_cos_{}.json".format(file)
#         save_dict(table,output)

# for subdir, dirs, files in os.walk(task_dir):
#     for file in files:
#         table = task_per_task(task_dir,model_tasks,file,"bert")
#         output = "../test_output/gpt2_kpi3_bert_{}.json".format(file)
#         save_dict(table,output)


# #STEP 7
# #---------------------------------------------------------------------------create augmented set------------------------------------------------------------     
#"""input: json file from STEP 1; output: dictionary of augmented process descriptions"""
# process_description = '../test_output/process_descriptions.json'                         
# content = open(process_description)
# data = json.load(content)
# content.close()

# table = {}    
# for key in data:
#     text = data[key]
#     augmented = augmentation_set(text)
#     table[key] = augmented
#     print("----PRINT--------------------",augmented)
# output = "../test_output/augmented_process_descriptions.json"
# save_dict(table,output)

# #STEP 8
# #--------------------------------------GET task list from augmented process descriptions --------------------------------------------------------------------
#"""input: json file from STEP 7; output: dictionary with list of tasks per augmented process description. Single file for each gpt model"""
# list_of_chats = {"gpt4":"gpt-4"}                                            
# f = open("../test_output/augmented_process_descriptions.json")
# data = json.load(f)
# f.close()
# aug_type=["eda_rep","eda_del","eda_swap","eda_ins","nplaug","tran_de","tran_ru","tran_es","emb"]
# for chat in list_of_chats:
#     table = {}    
#     for aug in aug_type:
#         for key in data:
#             text = data[key][aug]
#             tasks = extract_tasks(list_of_chats[chat],text,1)
#             table[key] = convert_to_list(tasks)
    
#         output = os.path.join("json_data/",aug + "_tasks_extraction.json")
#         save_dict(table,output)

# #STEP 9
# # # -----------------get amount of tasks from augmented data set ---------------------------------
#"""input: json files from STEP 8; output: data frame with the average number of tasks for each augmentation method"""
# augmented_dir = "../chat_tasks/augmented_tasks"
# data = get_augment_average(augmented_dir)
# print(data)


# #----------------------------------------get average number of tasks (model and llm)------------------------------------------------------------------
# from my_packages.average_data import get_average_model_tasks,get_average_chat_tasks
# model_tasks = "../test_output/bpmn_model_tasks.json"
# task_dir = '../tasks/' 
# extracted_models = get_average_model_tasks(model_tasks,"amount")
# chat_tasks = get_average_chat_tasks(task_dir)
# print(chat_tasks)

# #---------------------------------------------get aver number of words in task (model and llm)---------------------------------------
# from my_packages.average_data import get_average_model_words,get_average_chat_words
# model_tasks = "../test_output/bpmn_model_tasks.json"
# task_dir = '../tasks/'
# model_aver = get_average_model_words(model_tasks)
# chat_words = get_average_chat_words(task_dir)
# print(chat_words)

# #==============================================================================================
# #"This script generates text-based graph representation for process description"
# # rootdir = "../data_set/initial-cases"   #data set
# rootdir = "../pet_dataset/texts"
# savedir = "../pet_dataset/" #storage
# model = "text-davinci-001"                         #allowed models:"text-davinci-001","text-davinci-002","text-davinci-003" and "gpt-4", "gpt-3.5-turbo"
# prompt_type = "rules"                   #allowed prompt types: "simple", "rules", "bpmn", "activities", "rulact" and "rulact_bpmn"
# grahp_type = "mermaid.js"                #allowed types: graphviz, mermaid.js
# if "mermaid" in grahp_type: 
#     directory = "mermaid"
# else:
#     directory = "graphviz"

# path = "{}{}".format(savedir,model)
# for root, dirs, files in os.walk(rootdir):
#     for f in files:
#         if ".txt" in f:
#             print("============={}=====================".format(f))
#             file = "{}/{}".format(rootdir,f)
#             with open(file) as content:
#                 description = content.readlines()
#                 description = "".join(description)
#                 graph = generate_graph(description,prompt_type,grahp_type,model)
#                 file_name = "{}/{}".format(path,f)    
#                 os.makedirs(os.path.dirname(file_name), exist_ok=True) 
#                 with open(file_name, "w") as graph_file:
#                     graph_file.write(graph)

# #===================================extract tasks from mermaid or graphviz================================
# directory = '../pet_dataset/xml_to_mermaid' 
# data = {}
# for filename in os.listdir(directory):
#     f = os.path.join(directory, filename)
#     if os.path.isfile(f):
#         key = filename.split(".")[0]    
#         content = open(f, "r")
#         tasks = extract_mermaid_tasks(content)
#         data[key] = {"1":{"amount":len(tasks),"process":tasks,"subprocess": 1}}
# print(data)

# directory = '../pet_dataset/gv_examples' 
# data = {}
# for filename in os.listdir(directory):
#     f = os.path.join(directory, filename)
#     if os.path.isfile(f):
#         key = filename.split(".")[0]    
#         content = open(f, "r")
#         tasks = extract_graphviz_tasks(content)
#         data[key] = {"1":{"amount":len(tasks),"process":tasks,"subprocess": 1}}
# print(data)

# example_model = """
# digraph {
#   rankdir=LR;
#   "start_1"[shape=circle label=""];
#   "end_1"[shape=doublecircle label=""];
#   "seg_1"[shape=diamond label="X"];
#   "task_1"[shape=rectangle label="MPON sents the dismissal to the MPOO"];
#   "task_2"[shape=rectangle label="MPOO reviews the dismissal"];
#   "task_3"[shape=rectangle label="MPOO opposes the dismissal of MPON"];
#   "task_4"[shape=rectangle label="MPOO confirmes the dismissal of the MPON"];
#   "start_1" -> "task_1";
#   "task_1" -> "task_2";
#   "task_2" -> "seg_1";
#   "seg_1" -> "task_3"[label="Oppose"];
#   "seg_1" -> "task_4"[label="Confirm"];
#   "task_3" -> "end_1";
#   "task_4" -> "end_1";
# }
# """

# aaaa = example_model.splitlines()
# tasks = extract_graphviz_tasks(aaaa)
# print(tasks)

