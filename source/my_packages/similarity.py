import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sentence_transformers import SentenceTransformer, util
import numpy as np
import json
import requests
# import torch


def get_cosine(text1,text2):
    corpus = [text1,text2]
    vectorizer = TfidfVectorizer()
    trsfm=vectorizer.fit_transform(corpus)
    cos_sim = cosine_similarity(trsfm[0:1], trsfm)
    cos_sim = cos_sim[0][1]
    return cos_sim

def sts_bert(t1,t2):
    model = SentenceTransformer('sentence-transformers/stsb-mpnet-base-v2')
    sentences = [t1, t2]
    embedding_1= model.encode(sentences[0], convert_to_tensor=True)
    embedding_2 = model.encode(sentences[1], convert_to_tensor=True)
    score = util.pytorch_cos_sim(embedding_1, embedding_2)
    score = score.tolist()
    return score[0][0]

def sts_multi_bert(sim_model,embedding_1,t2):
    embedding_2 = sim_model.encode(t2, convert_to_tensor=True)
    score = util.pytorch_cos_sim(embedding_1, embedding_2)
    score = score.tolist()
    return score[0][0]

def create_matrix(sim_model,chat,model,type):
    final = []
    for m in model:
        if type != "cos":
            embedding_1= sim_model.encode(m, convert_to_tensor=True)
        row = []
        for c in chat:
            if type == "cos":
                val = get_cosine(m,c)
            else:
                val = sts_multi_bert(sim_model,embedding_1,c)
            row.append(val)
        final.append(row)
    new = pd.DataFrame(final)
    return new

def find_match(new,model,thold):
    myrange = len(model)
    table = []
    for r in range(0,myrange):
        i,j = new.stack().index[np.argmax(new.values)]
        score = new[j][i]
        new = new.drop([i], axis=0)
        if score> thold:
            text = {"score":score,"chat":j,"model":i}
            table.append(text)
    #table = dict(sorted(table.items()))
    return table

def matrix_output(chat,model,type="cos",sim_model=0):
    matrix = create_matrix(sim_model,chat,model,type)
    if type == "cos":
        table = find_match(matrix,model,0.2)
    else:
        table = find_match(matrix,model,0.5)
    if table:
        if table is not None:
            tasks_model = len(table)
            aver_score = 0
            for t in table:
                aver_score = t["score"] + aver_score
            aver_score = aver_score/tasks_model

            temp = []
            for t in table:
                if t["chat"] not in temp:
                    temp.append(t["chat"])
            tasks_chat = len(temp)
            return {"orig_tasks_model":len(model),"orig_tasks_chat":len(chat), "tasks_model":tasks_model,"tasks_chat":tasks_chat, "aver_score":aver_score}
        else:   #print(tasks_model,tasks_chat,aver_score)
            return {"orig_tasks_model":len(model),"orig_tasks_chat":len(chat),"tasks_model":0,"tasks_chat":0, "aver_score":0}
    return {"orig_tasks_model":len(model),"orig_tasks_chat":len(chat),"tasks_model":0,"tasks_chat":0, "aver_score":0}










