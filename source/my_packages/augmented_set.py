from textaugment import EDA
from textaugment import Translate
import nlpaug.augmenter.word as naw
from nltk import tokenize
# import torch
import random
from textattack.augmentation import EmbeddingAugmenter


def define_n(text):
    words = text.split()
    maxi = int(len(words)/2)
    if maxi >1:
        n = random.randrange(1, maxi)
    else:
        n =1
    return n 

#-------------------EDA-----------------
def eda_function(text, type="rep", p=0.2):
    eda_augment = EDA()
    n = define_n(text)
    if type == "rep":
        result = eda_augment.synonym_replacement(text,n=n)
    elif type == "del":
        result = eda_augment.random_deletion(text,p=p)
    elif type == "swap":
        result = eda_augment.random_swap(text,n=n)
    elif type == "ins":
        result = eda_augment.random_insertion(text,n=n)
    return result

#-------------------transale nltk--------------------
def translate_function(text,to):   #de,en,fr,es,ru
    t = Translate(src="en", to=to)
    result = t.augment(text)
    return result

#-------------------nlpaug--------------------
def nlpaug_function(text):
    syn_aug = naw.SynonymAug(aug_src='wordnet',aug_max=2)
    result = syn_aug.augment(text,n=1)
    return result[0]

#-------------------embedding--------------------------
def embedding(original):
    embed_aug = EmbeddingAugmenter(transformations_per_example=5)
    resutl = embed_aug.augment(original)
    return resutl[0]



#------------------------ MAIN ------------------
def loop_function(original,aug_type):
    sentences = tokenize.sent_tokenize(original)
    augmented = ""
    for s in sentences:
        if len(s) > 3:
            print(s)
            s = s.replace("\n", "")
            s = s.replace("\n\n", "")
            if aug_type == "eda_rep":
                syn_augmented = eda_function(s,"rep")
            elif aug_type == "eda_del": 
                syn_augmented = eda_function(s,"del",p=0.2)
            elif aug_type == "eda_swap": 
                syn_augmented = eda_function(s,"swap")
            elif aug_type == "eda_ins": 
                syn_augmented = eda_function(s,"ins")
            elif aug_type == "nplaug": 
                syn_augmented = nlpaug_function(s)
            elif aug_type == "tran_de": 
                syn_augmented = translate_function(s,"de")
            elif aug_type == "tran_ru": 
                syn_augmented = translate_function(s,"ru")
            elif aug_type == "tran_es": 
                syn_augmented = translate_function(s,"es")
            elif aug_type == "emb": 
                syn_augmented = embedding(s)
            print(s,syn_augmented)
            try:
                augmented = augmented + syn_augmented
            except:
                augmented = augmented + syn_augmented[0]
        
    return augmented

def augmentation_set(original):
    context = {}
    aug_type=["eda_rep","eda_del","eda_swap","eda_ins","nplaug","tran_de","tran_ru","tran_es","emb"]     #"tran_aug", "pegassus" ,"t5"
    for t in aug_type:
        print(t)
        augmented = loop_function(original,t)
        context[t] = augmented
    return context










