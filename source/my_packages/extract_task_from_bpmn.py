import xmltodict
import os
import json  


"""Extract tasks from models to generate process description"""

def save_task_to_list(key,v,value,task_list):
    if key == "endEvent":
        task_list[v] = {"task":key,"next":["END"],"direction":"Converging","type":key}
    elif "gateway" in key.lower():
        if type(value["outgoing"]) != list :
            value["outgoing"] = [value["outgoing"]]
        task_list[v] = {"task":key,"next":value["outgoing"],"direction":value["@gatewayDirection"],"type":key}  
    else:                                          #not a list
        try:
            if type(value["outgoing"]) != list :
                value["outgoing"] = [value["outgoing"]]
            task_list[v] = {"task":value["@name"],"next":value["outgoing"],"direction":"NA","type": key}
        except:
            a =1
    return True

def get_start_event(process,path_to_file):
    start_event = None
    if "startEvent" in process:
        start_body = process["startEvent"]
        if "outgoing" in start_body:
            try:
                start_event = start_body["outgoing"]
            except:
                print(path_to_file, "Start event is not connected")
        else:
            try:
                start_event = start_body[0]["outgoing"]
            except:
                print(path_to_file, "No start event")
    else:
        print(path_to_file, "No start event")
    return start_event

def get_task_list(process,path_to_file,start_event):
    task_list = {}
    if start_event == None:
        print(path_to_file, "No outgoing tasks from start event")
        return None
    else:
        for key, value in process.items():
            if type(value) == list:
                for v in value:
                    if "incoming" in v:
                        if type(v["incoming"]) == list:
                            for i in v["incoming"]:
                                save_task_to_list(key,i,v,task_list)
                        else:
                            save_task_to_list(key,v["incoming"],v,task_list)
            else:
                try:
                    if "incoming" in value and value != None:
                        if type(value["incoming"]) == list:
                            for i in value["incoming"]:
                                save_task_to_list(key,i,value,task_list)
                        else:
                            save_task_to_list(key,value["incoming"],value,task_list)
                except:
                    pass      

    return task_list


def append_text(linetext,body):
    if "task" in body["type"].lower() or "process" in body["type"].lower():
        #if body["task"] not in linetext:
        linetext.append(body["task"])

def get_closed_gateway(start_event,task_list,path_to_file,passed_sids,text):
    stop = []
    for a in range(0,len(start_event)):
        s = start_event[a]
        if task_list[s] in stop:    #addeed
            continue
        if s not in passed_sids:
            append_text(text,task_list[s])
            passed_sids.append(s)
            next_task = task_list[s]
            if task_list[s]["direction"] == "Converging":   # added
                stop.append(task_list[s])
                continue
            while True:
                new_task = get_next(next_task,task_list,path_to_file,passed_sids,text)
                if new_task == None:
                    break
                if new_task ==  {'task': 'endEvent', 'next': ['END'], 'direction': 'Converging', 'type': 'endEvent'}:
                    break
                if new_task["direction"] == "Converging":
                    if new_task["task"] != "":
                        if new_task["task"] != "endEvent":
                            stop.append(new_task)
                            break
                next_task = new_task          
        else:
            print("Already passed",s)
    return stop[0]

def get_next(start_event,task_list,path_to_file,passed_sids,text):
    if start_event == None:
        return None
    else:
        sids = start_event["next"]
        if len(sids) == 1:
            sid = sids[0]
            if sid != "END":
                if sid not in passed_sids:
                    try:
                        passed_sids.append(sid)
                        new_sids = task_list[sid]
                        append_text(text,new_sids)
                    except:
                        print("User error")
                        new_sids = {'task': 'endEvent', 'next': ['END'], 'direction': 'Converging', 'type': 'endEvent'}
                else:
                    return None
            else: 
                new_sids = {'task': 'endEvent', 'next': ['END'], 'direction': 'Converging', 'type': 'endEvent'}
        elif len(sids) > 1:
            closed = get_closed_gateway(sids,task_list,path_to_file,passed_sids,text)
            new_sids = closed
        else:
            print([path_to_file,"No next task is passed"])
    return new_sids


def new_function(process,path_to_file):
    text = []
    passed_sids = []
    start_event =get_start_event(process,path_to_file)
    task_list = get_task_list(process,path_to_file,start_event)
    if task_list != None:
        start_event = {"next":[start_event]}
        while True:
            try:
                start_event = get_next(start_event,task_list,path_to_file,passed_sids,text)
            except:
                print("User error")
                start_event = None
            if start_event == None:
                break
            if start_event ==  {'task': 'endEvent', 'next': ['END'], 'direction': 'Converging', 'type': 'endEvent'}:
                break
        return text
    else:
        return text


def find_all_tasks(path_to_file):
    if ".xml" in path_to_file:   
        print("----------------------------", path_to_file)
        with open(path_to_file) as xml_file:
            data_dict = xmltodict.parse(xml_file.read())
            process = data_dict["definitions"]["process"]
            if type(process) != list: 
                text = new_function(process,path_to_file)
                text =  list(dict.fromkeys(text))
                if text !=None:
                    result = {"amount":len(text),"process":text,"subprocess":1}
            else:
                sum = 0 
                subcount = 0
                whole_text = []
                for sub in process:
                    text = new_function(sub,path_to_file)
                    if text !=None:
                        sum = len(text) + sum
                        subcount = subcount + 1
                        whole_text.extend(text)
                whole_text =  list(dict.fromkeys(whole_text))
                result = {"amount":sum,"process":whole_text,"subprocess":subcount}
        print(result)
        return result
    else:
        return None