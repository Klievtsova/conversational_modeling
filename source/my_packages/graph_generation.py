from my_packages.rules import mermaid, graphviz, bpmn
from my_packages.chatbot_api import  ask_gpt, extract_tasks
import os
import re

#"This script generates text-based graph representation for process description"

def create_prompt(prompt_type,description,grahp_type,set_of_rules="None",bpmn="None",activities="None"):
    list_of_prompts= {
        "simple": "Process description: {}. Considering provided process description create a valid {}.",
        "rules": "Process description: {}. {}. Considering provided process description and a set of custom rules create a valid {} graph.",
        "bpmn": "Process description: {}. {} Considering provided process description and a short description of BPMN standard create a valid {} graph.",
        "activities": "Process description: {}. List of activities: {}. Considering provided process description and a list of activities create a valid {} graph.",
        "rulact": "Process description: {}. {}. List of activities: {}. Considering provided process description, a set of custom rules and a list of activities create a valid {} graph.",
        "rulact_bpmn": "Process description: {}. {}. List of activities: {}. {}. Considering provided process description, a set of custom rules, a list of activities and a short description of BPMN standard create a valid {} graph."
    }

    prompt_config = {
        "simple":[description,grahp_type],
        "rules":[description,set_of_rules,grahp_type],
        "bpmn":[description,bpmn,grahp_type],
        "activities":[description,activities,grahp_type],
        "rulact": [description,set_of_rules,activities,grahp_type],
        "rulact_bpmn": [description,set_of_rules,activities,bpmn,grahp_type]
    }
    prompt = list_of_prompts[prompt_type].format(*prompt_config[prompt_type])
    prompt += "Only a valid graph without any additional text or info must be returned."
    return prompt

def generate_graph(description,prompt_type,grahp_type,model):
    if "mermaid" in grahp_type: 
        set_of_rules = mermaid
    else:
        set_of_rules = graphviz
    if prompt_type == "rules" or prompt_type=="simple" or prompt_type=="bpmn":  
        prompt = create_prompt(prompt_type,description,grahp_type,set_of_rules,bpmn)
    else:
        activities = extract_tasks(model,description,1)
        prompt = create_prompt(prompt_type,description,grahp_type,set_of_rules,bpmn,activities)

    response = ask_gpt(prompt,model)  
    if response == "smth went wrong":
        return("Error")
    else:
        return response

"""parse generated graphs"""
def get_task_nodes(t,reg_ex):
    try:
        label = re.findall(reg_ex, t)
        if label:
            return label[0]
    except:
        return None

def extract_mermaid_tasks(content): 
    elements = []
    try:
        for c in content:
            tasks = c.split("-->")
            for t in tasks:
                if "task" in t:
                    label = get_task_nodes(t,"\((.*?)\)")
                    if label:
                        elements.append(label)
    except:
        pass
    return elements

def extract_graphviz_tasks(content):
    elements = []
    try:
        for c in content:
            if "rectangle" in c:
                if "label" in c:
                    new = c.split("label=")[-1]
                    label = get_task_nodes(new,'\"(.*?)\"')
                else:
                    label = get_task_nodes(c,'\"(.*?)\"')
                if label:
                    elements.append(label)
    except:
        pass
    return elements



    #'\label="(.*?)\"'