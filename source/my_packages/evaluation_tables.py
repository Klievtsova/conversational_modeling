import json
import os
from my_packages.kpis import text_similarity, set_similatiry, set_overlap
from sentence_transformers import SentenceTransformer

"""
function is working only with files, that were generated using descriptions_to_json() and find_all_tasks()
generate json data for kpi1 and kpi4
input: directory with generated tasks, file with extracted process description and
type of similarity: "cos" (noncontextual) and "bert" (contextual, i.e., semantic)
"""
def compare_pd_llm(rootdir,input,type):
    output = {}
    f = open(input)  
    original_data = json.load(f)
    for root, dirs, files in os.walk(rootdir):
        for f in files:
            chat_name = f.split(".")[0]
            path_to_file = os.path.join(root, f)
            extracted = open(path_to_file)
            extracted = json.load(extracted)
            for key in extracted:
                if key not in output:
                    output[key] = {}
                try:
                    description = original_data[key]
                    llm_tasks = extracted[key]["tasks"]
                    llm_tasks = '.'.join(llm_tasks)
                    kpi = text_similarity(description,llm_tasks,type)
                    kpi = kpi["text_similarity"]
                    output[key].update({chat_name:kpi})
                except:
                    pass
    return output

def compare_llm_bpmn(model_task,gptdir,type="cos"):
    table = {}
    f = open(model_task)  
    models = json.load(f)
    for m in models:
        try:
            table[m] = {}
            table.update({m:{"chats":[]}})
            for root, dirs, files in os.walk(gptdir):
                for f in files:
                    path_to_file = os.path.join(root, f)
                    extracted = open(path_to_file)
                    extracted_chat = json.load(extracted)
                    llm_tasks = extracted_chat[m]["tasks"]
                    bpmn_tasks_list = models[m]
                    test = set_similatiry(llm_tasks,bpmn_tasks_list,type)
                    new_key = f.split(".")[0]
                    table[m]["chats"].append({new_key:test})
        except:
            pass
    return table

def task_per_task(gptdir,model_task,file,type):
    if type != "cos":
        sim_model = SentenceTransformer('sentence-transformers/stsb-mpnet-base-v2')
    else:
        sim_model = 0
    f = open(model_task)  
    models = json.load(f)
    table = {}
    chat = os.path.join(gptdir, file)
    f = open(chat)  
    chat_tasks = json.load(f)
    for key,value in chat_tasks.items():
        table[key] = []
        llm_tasks = value["tasks"]
        # try:
        for k,v in models[key].items():
            bpmn_tasks = v["process"]
            result = set_overlap(llm_tasks,bpmn_tasks,type,sim_model)
            table[key]=result
        # except:
            # pass
    return table 

