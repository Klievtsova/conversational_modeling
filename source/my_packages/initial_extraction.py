import re
import os
import json
import json
import xmltodict
import pandas as pd
from collections import Counter



"""This function saves dictionary as a json file"""
def save_dict(data,output):
    json_data = json.dumps(data, indent=4)
    with open(output, "w") as outfile:
        outfile.write(json_data)

"""Collect text from each file in a folder and convert it to one dictionary.
input: rootdir = directory with files; 
output: dictionary, where key = file_name, value = file content"""
def descriptions_to_json(rootdir):
	try:
		data = {}
		for subdir, dirs, files in os.walk(rootdir):
			for file in files:
				file_path = os.path.join(subdir, file)
				if os.path.isfile(file_path):
					case_name = file.split(".")[0]
					content = open(file_path, "r",encoding="utf-8")  
					description = content.read()
					description = description.replace("\n", "")
					description = description.replace("\n\n", "")
					data[case_name] = description
		return data
	except Exception as error:
		print("Error: {}".format(error))
		return data	


""""Get basic information from bpmn models"""

def count_event(value):
    count = 0
    try:
        if type(value) == list:
            for v in value:
                count = count + 1
        else:
            count = count + 1
    except:
        return count
    return count

def count_parallel(value):
    count = 0
    try:
        if type(value) == list:
            for v in value:
                if v["@gatewayDirection"] == "Diverging" or v["@gatewayDirection"] == "Unspecified" or v["@gatewayDirection"] == "Mixed":
                    count = count + 1
        else:
            count = count + 1
    except:
        return count
    return count

def count_exclusive(value):
    count = 0
    if type(value) == list:
        for v in value:
            if v["@gatewayDirection"] == "Diverging" or v["@gatewayDirection"] == "Unspecified" or v["@gatewayDirection"] == "Mixed":
                count = count + 1
    else:
        count = count + 1
    return count

def count_task(value,count):
    if type(value) == list:
        for v in value:
            count = count + 1
    else:
        count = count + 1
    return count

def count_model(process):
    output = {}
    tasks = 0
    for key, value in process.items():
        if key == "startEvent":
            starts = count_event(value)
            output["start"] = starts
        if key == "endEvent":
            ends = count_event(value)
            output["end"] = ends
        if "subprocess" in key.lower():
            tasks = count_task(value,tasks)
        if "task" in key.lower():
            tasks = count_task(value,tasks)
        if key == "parallelGateway":
            parallels = count_parallel(value)
            output["parallel"] = parallels    
        if key == "exclusiveGateway":
            exclusive = count_exclusive(value)
            output["exclusive"] = exclusive 
    output["tasks"] = tasks
    return output

def get_quality(new_root,f):
    path_to_file = new_root + f
    with open(path_to_file,mode='r') as doc:
        quality = doc.read()
        doc.close()
    return quality

def get_basic_quality_information(key_name,f,table,new_root):
    counter =  f.split(".")[0]
    if key_name not in table:
        table[key_name] = {}
        table[key_name].update({counter:{}})
    else:
        if counter not in table[key_name]:
            table[key_name].update({counter:{}})
    if ".xml" in f:
        path_to_file = new_root + f
        result = model_basic_info(path_to_file)
        table[key_name][counter].update(result)
    else:
        quality = get_quality(new_root,f)
        table[key_name][counter].update({"quality":quality})
    return table


"""count number of start and end events, exclusive and parallel gateways and tasks in a model (bpmn 2.0 standard)"""
def model_basic_info(path_to_file):
    with open(path_to_file) as xml_file:
        data_dict = xmltodict.parse(xml_file.read())
        process = data_dict["definitions"]["process"] 
        try:
            result = count_model(process)                    
        except:
            result = {}
            for sub in process:
                for key, value in sub.items():
                    tmp = count_model(sub)
                result = dict(Counter(tmp)+Counter(result))   
    return result

"""return list of basic elemtns for all models in the folder"""
def get_basic_information(new_root):
    table = {}
    for subdir, dirs, files in os.walk(new_root):
        for f in files:
            counter =  f.split(".")[0]
            if ".xml" in f:
                path_to_file = new_root + f
                result = model_basic_info(path_to_file)
                table[counter] = result
    return table