mermaid = """
Rules for mermaid js flowcharts:
The graph must use the LR (Left to Right) direction.
Each mermaid js node must have the following structure:
id:type:shape and text
    id - it is a unique identifier. Integer from 1 to n. Each node has a unique identifier
    type - defines the type of the element regarding to BPMN 2.0 notation. 
        possible types are: start event, end event, task, exclusive gateway and parallel gateway. 
        Based on the type of the node following shapes and texts are to be used:
        startevent: ((startevent))     i.e., id:startevent:((startevent)) 
        endevent: ((endevent))	     i.e., id:endevent:((endevent)) 
        task: (task label)             i.e., id:task:(task label)
        exclusivegateway: {x}          i.e., id:exclusivegateway:{x} 
        parallelgateway: {AND}         i.e., id:exclusivegateway:{AND} 

All nodes that have occurred more than once should have following structure: id:type: (i.e., 2:task: ) by futher occurrence. 
It is strictly prohibited to use only id (i.e. 2) as a reference.   

    all elements are connected with each other with the help of the direction.
        direction: -->
    if there are some conditions or annotations it is necessary to use text on links (i.e., edge labels)
        edge label: |condition or annotation|
    edge label is always located between 2 nodes: id:exclusivegateway:{x} --> |condition or annotation|id:task:(task label)
"""

graphviz = """
Graphviz rules: 
Each graph must have LR (Left to Right) direction and consists of nodes and edges. 
Each node has following structure: "name"[attributes]
There are 5 diferent types of nodes: start event, end event, task, exclusive gateway and parallel gateway. 
Each node has its specific attributes based on the type of the node. 
  start node:        "start_1"[shape=circle label=""]; 
  end node:          "end_1"[shape=doublecircle label=""];
In both start and end nodes labels are always empty. 
  task:              "task label"[shape=rectangle]; 
Task labels are always unique.
  exclusive gateway: "seg_1"[shape=diamond label="X"];  
  parallel gateway:  "spg_1"[shape=diamond label="AND"]; 

Gateways are not tasks, they just indicate that the control flow of the process is splitted or merged.
Following names "seg_1" and "meg_1" should be used for splitting and merging exclusive gateways.
Following names "spg_1" and "mpg_1" should be used for splitting and merging parallel gateways.

Each time when new start, end or gateways node is used, the counter should be incrimented at 1. 

all elements are connected with each other with the help of the edges.
  edge: ->
examples: 
"start" -> "task 1"
"task 1" -> "task 2"

if there are some conditions or annotations it is necessary to use text on links (i.e., edge labels)
    edge label:  "task 1" -> "task 2"[label="condition 1"]
"""

bpmn = """
BPMN standard:
Each model has only 1 Start event, 1 or more End events, activities (i.e. tasks) and gateways. 
A Start Event shows where a Process begins. A Start Event has exactly one outgoing element and no incomming elements. 
An End Event marks where a Process ends. End event has 1 incomming element and no outgoing elements.
Between start and end events there are multiple tasks and pathes.  
Path represents one of the scenarios, how the process could be executed.
Task represents the work performed within a process. A task will normally take some time to perform, will involve one or more resources, will usually require some type of input, and will usually produce some sort of output. Each task has a label, where no special characters are allowed.
Gateways are modeling elements that control the paths within the Process. Gateways themselves are not activities, they just split and merge the flow of a Process. 
Gateways are unnecessary if the Sequence Flow does not require controlling. The two most commonly used Gateways are Exclusive and Parallel. 
If after some particular task decision is to be made (i.e. alternative pathes occure), splitting exclusive gateway to be used. 
Only one of multiple outgoing pathes (exclusively) will be executed. After that process flow should be merged again with the help of merging exclusive gateway. 
If after some particular task multiple tasks should be executed at the same time, splitting parallel gateway to be used. 
During execution of parallel tasks thare is always a synchronization point (waiting for several separate paths to reach a certain point before the process can continue). 
This synchronization point is represented by mergins parallel gateway. 
Each split should be always merges. Only gateways of the same type can be used for splitting and merging. 
"""


