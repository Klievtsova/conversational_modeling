import openai
import requests
import json

openai.api_key = "<YOUR_KEY>"
parameters = {
    "temperature":0,
    "max_tokens":1024,
    "top_p": 1
}

def ask_gpt_complete(prompt,model,parameters):
    try:
        response = openai.Completion.create(
            engine=model,
            prompt = prompt,
            max_tokens=parameters["max_tokens"],
            temperature = parameters["temperature"],
            top_p = parameters["top_p"],
            stop = ["ooooooooooo"]
            )
        return response.choices[0].text
    except Exception as e:
        print("smth went wrong",e)
        return "smth went wrong"

def ask_gpt_chat(prompt,model,parameters):   
    try:
        response = openai.ChatCompletion.create(
            model=model,
            messages=[
                {
                "role": "user",
                "content": prompt
                }
            ],
            temperature=parameters["temperature"],
            max_tokens=parameters["max_tokens"],
            top_p=parameters["top_p"],
            frequency_penalty=0,
            presence_penalty=0
        )
        return response.choices[0].message.content
    except Exception as e:
        print("smth went wrong",e)
        return "smth went wrong"   

def ask_gpt(prompt,model):
    if "text-davinci" in model:
        response = ask_gpt_complete(prompt,model,parameters)
    else:
        response = ask_gpt_chat(prompt,model,parameters)
    return response

def extract_tasks(model,original,restricted):
    try:
        if restricted == 0:
            prompt = "Considering following process description [{}] return the list of main tasks in it (each from new line).".format(original) 
        else:
            prompt = "Considering following process description [{}]  return the list of activities (each 3-5 words maximum) one by one without any additional information.".format(original) 
        response = ask_gpt(prompt,model)
        return response
    except Exception as e:
        print("smth went wrong",e)
        return "smth went wrong"

def convert_to_list(tasks):
    task_list = tasks.split("\n")
    task_list = [x for x in task_list if x != '']
    amount = len(task_list)
    result = {"amount":amount,"tasks":task_list}
    return result