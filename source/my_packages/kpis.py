from my_packages.similarity import get_cosine,sts_bert,matrix_output

def get_similarity(text1,text2,type="cos"):
    if type == "cos":
        similarity = get_cosine(text1,text2)
    else:
        similarity = sts_bert(text1,text2)
    return similarity

"""both description and llm_tasks are string values"""
def text_similarity(description,llm_tasks,type="cos"):
    similarity = get_similarity(description,llm_tasks,type)
    return {"text_similarity":similarity}

"""llm_tasks is a list, bpmn_task_list is a dictionaty of lists (length: 1..n)"""
def set_similatiry(llm_tasks,bpmn_tasks_list,type="cos"):
    kpis = []
    bpmn_lens = []
    llm_length = len(llm_tasks)
    sllm_tasks = '. '.join(llm_tasks)
    for bpmn_tasks in bpmn_tasks_list:
        bpmn_length = bpmn_tasks_list[bpmn_tasks]["amount"]
        sbpmn_tasks = '. '.join(bpmn_tasks_list[bpmn_tasks]["process"])
        similarity = get_similarity(sllm_tasks,sbpmn_tasks,type)
        kpis.append(similarity)
        bpmn_lens.append(bpmn_length)
    kpi = sum(kpis)/len(kpis)
    aver_bpmn = sum(bpmn_lens)/len(bpmn_lens)
    return {"set_similarity":kpi,"number_tasks_llm":llm_length,"number_tasks_bpmn":aver_bpmn}

"""llm_tasks is a list, bpmn_task_list is a dictionaty of lists (length: 1..n)"""
def simple_set_similatiry(llm_tasks,bpmn_tasks,type="cos"):
    llm_length = len(llm_tasks)
    sllm_tasks = '. '.join(llm_tasks)
    bpmn_length = len(bpmn_tasks)
    sbpmn_tasks = '. '.join(bpmn_tasks)
    similarity = get_similarity(sllm_tasks,sbpmn_tasks,type)
    return {"set_similarity":similarity,"number_tasks_llm":llm_length,"number_tasks_bpmn":bpmn_length}

"""bpmn_tasks and llm_tasks are lists of strings"""
def set_overlap(llm_tasks,bpmn_tasks,type="cos",sim_model=0):
    if len(llm_tasks)==0 and len(bpmn_tasks)==0:
        return {"common_model":0, "common_llm":0, "only_model": 0, "only_llm": 0}
    elif len(llm_tasks)==0:
        return {"common_model":0, "common_llm":0, "only_model": len(bpmn_tasks), "only_llm": 0}
    elif len(bpmn_tasks)==0:
        return {"common_model":0, "common_llm":0, "only_model": 0, "only_llm": len(llm_tasks)}
    overlap = matrix_output(llm_tasks,bpmn_tasks,type,sim_model)
    only_model = overlap["orig_tasks_model"] - overlap["tasks_model"]
    only_llm = overlap["orig_tasks_chat"] - overlap["tasks_chat"]
    kpis = {"common_model":overlap["tasks_model"], "common_llm":overlap["tasks_chat"], "only_model": only_model, "only_llm": only_llm}
    return kpis


        