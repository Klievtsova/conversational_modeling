

1:startevent: Employee submits a vacation request
2:task: Supervisor approves or rejects the request
3:exclusivegateway: If the request is rejected, the application is returned to the applicant / employee who can review the rejection reasons
4:parallelgateway: If the request is approved, a notification is generated to the Human Resources Representative, who must complete the respective management procedures.